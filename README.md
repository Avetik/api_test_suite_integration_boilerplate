# api_test_suite_integration_boilerplate

### Mini project to test CI/CD


*  The project creates a job, for the every commit, that pulls the latest version of api-test-suite and runs it.
*  Later in the job `artifacts` it generates the report file. You download it, find the `.html` file and open it.
*  Now the projects runs only permission and positive tests. 

To integrate negative tests: `npm run negative-tests` must be added under the `npm run positive-tests`.
